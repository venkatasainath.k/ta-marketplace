/*!
 * Start Bootstrap - Grayscale Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery to collapse the navbar on scroll
$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

// remove the focused state after click,
// otherwise bootstrap will still highlight the link
$("a").mouseup(function(){
    $(this).blur();
})

//constructing html for project tiles dynamically
$(document).ready( function(){
    $.ajax({
        async: true,
        headers: {
            'PRIVATE-TOKEN':'iixe2xZiuJDnrq89PDqq'
        },
        url: "https://tagitlab.techaspect.com/api/v4/projects",
        type: "GET",
        cache: false,
        dataType: "json",
        success: function (data) {
            var responseData = JSON.parse(JSON.stringify(data));
            var element = "";
            for(var i=0;i<responseData.length;i++){
                element += "<div class='col-lg-4 col-sm-6'>";
                element += "<a href='/ta-marketplace/detail' target='_blank' class='portfolio-box'>";
                element += "<img src='img/portfolio/3.jpg' class='img-responsive' alt='img'>";
                element += "<div class='portfolio-box-caption'>";
                element += "<div class='portfolio-box-caption-content'>";
                element += "<div class='project-category text-faded'>Project</div>";
                element += "<div class='project-name'>"+responseData[i].name+"</div></div></div></a></div>";

                var projectName = $('#project-name').val();
                if(projectName == responseData[i].name) {
                    $('#fork-me').attr("href",responseData[i].web_url);
                }

            }
            if($("#project-tiles-container").get(0)){
                document.getElementById("project-tiles-container").innerHTML = element;
            }
        },
        error: function(e) {
            console.log(e);
        } 

    }); 
});
